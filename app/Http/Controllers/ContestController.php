<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ContestModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ContestController extends Controller
{
    public function save(Request $request)
    {        
        $validator = Validator::make(
            $request->all(),
			[
                'name'         => 'required',
                'date_from'    => 'required',
                'date_to'      => 'required',
            ]
        );
        if ($validator->fails())
            return response()->json($validator->errors(), 422);


        $where = [
            'name'         => filter_var($request->name,FILTER_SANITIZE_STRING),
            'description'  => filter_var($request->description,FILTER_SANITIZE_STRING),
            'date_from'    => Carbon::parse($request->date_from),
            'date_to'      => Carbon::parse($request->date_to),
            'admin'        => Auth::user()->username,
        ];

        try {
            $contest = ContestModel::UPDATEORCREATE(
                [ 'id' => $request->id ],
                $where
            );

            if($contest)
                return response()->json(['message' => 'New Informations is saved'], 200);
            else
                return response()->json(['message' => 'There was a problem processing your request'], 500);
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getAllContest(Request $request){
        $where = [];
        $role = $request->role;

        array_push($where, ['c.status', 1]);
        
        if($role > 1)
            array_push($where, ['c.admin', Auth::user()->username]);

        if($request->country != '')
            array_push($where, ['c.country_id', $request->country]);

        return $contest_data = DB::table('contest as c')
                        ->select('c.*')
                        ->where($where)
                        ->orderBy('c.id', 'desc')
                        ->get();
        
    }    
    public function getContestInfo(Request $request){
        return ContestModel::WHERE('id', $request->contest_id)->FIRST();
    }
    public function delete(Request $request){
        $id = isset($request->id) ? $request->id : NULL;
        $contest = ContestModel::WHERE('id', $id)->FIRST();        

        if(empty($contest))
            return response()->json(['message' => 'Invalid Reference Key'], 422);
        try{
            $contest = ContestModel::UPDATEORCREATE(
                [ 'id' => $request->id ],
                [
                    'status'    => 2,                    
                ]
            );
            if($contest)
				return response()->json(['message' => 'Contest is removed'], 200);
			else
				return response()->json(['message' => 'There was a problem processing your request'], 500);
        }
        catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
    }
}
