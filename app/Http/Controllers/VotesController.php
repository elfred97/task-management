<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Models;
use App\Models\VotesModel;
use App\Models\DJModel;
use App\Models\ContestModel;
use App\Models\CountryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VotesController extends Controller
{
    public function voteDJ(Request $request)
    {
        $today   = Carbon::now();
        $contest = ContestModel::WHERE('id', $request->contest_id)->FIRST();
        $country = CountryModel::WHERE('id', $contest->country_id)->FIRST();
        // Check for the vote today by this IP if it passes the refresh time
        $votes = VotesModel::WHERE([['ip_address', $request->ip_address], ['contest_id', $request->contest_id]])->LATEST('created_at')->FIRST();
        $refresh_time = Carbon::parse($contest->refresh_time)->format('H:i:s');
        
        if($votes){
            
            $vote_date = Carbon::parse($votes->created_at);
            $vote_time = Carbon::parse($votes->created_at)->format('H:i:s');
            
            if($vote_date->diff($today)->days > 0){
                if($today->format('H:i:s') > $refresh_time){
                    return $this->saveVote($request->dj_id, $request->contest_id, $request->ip_address);
                }
                else
                    return response()->json(['message' => 'You can only vote once per day'], 200);
            }
            else
                return response()->json(['message' => 'You can only vote once per day'], 200);
        }
        else
            return $this->saveVote($request->dj_id, $request->contest_id, $request->ip_address);       
    }
    public function getDJVote(Request $request){
        $contest_id = $request->contest_id;

        return $votes = DB::TABLE('votes')->SELECT('*')->WHERE('contest_id', $contest_id)->GET();
    }
    public function saveVote($dj_id, $contest_id, $ip_address){
        try {
            $vote_created = VotesModel::CREATE(
                [
                    'dj_id'   => $dj_id,
                    'contest_id'=> $contest_id,
                    'ip_address' => $ip_address,
                ]
            );
            if($vote_created)
                return response()->json(['message' => 'Vote uploaded'], 200);
            else
                return response()->json(['message' => 'There was a problem processing your request'], 500);
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getVotesContest(Request $request){
        $contest_id = $request->contest_id;

        $query = DB::table('dj')
                ->select(DB::raw('COUNT(votes.id) as vote_count, dj.name'))
                ->where([['dj.contest_id', '=', $contest_id], ['dj.status', '=', 1]])
                ->leftJoin('votes', 'dj.id', '=', 'votes.dj_id')
                ->groupBy('dj.name')
                ->orderBy('vote_count', 'desc')
                ->get();

        return $query;
    }
    public function getTotalContestVotes(Request $request){
        $where = [];
        $where2 = [];

        array_push($where, ['contest.status', 1]);
        array_push($where, ['dj.status', 1]);
        if($request->country != '')
        array_push($where, ['contest.country_id', $request->country]);
        
        $total_votes = DB::TABLE('contest')
                        ->SELECT(DB::RAW('COUNT(votes.id) as vote_count, contest.id'))
                        ->leftJoin('votes', 'contest.id', '=', 'votes.contest_id')
                        ->leftJoin('dj', 'votes.dj_id', '=', 'dj.id')
                        ->groupBy('contest.id')
                        ->WHERE($where)
                        ->get();
        $total_dj    = DB::TABLE('contest')
                        ->SELECT(DB::RAW('COUNT(dj.id) as dj_count, contest.id'))
                        ->leftJoin('dj', 'contest.id', '=', 'dj.contest_id')
                        ->groupBy('contest.id')
                        ->WHERE($where)
                        ->get();
        return $this->buildJson(['total_votes' => $total_votes, 'total_dj' => $total_dj]);
    }
    public function getDailyVote(Request $request){
        
        try {
            $contestant_id = $request->contestant_id;
            $date       = $request->date;
    
            $where = [];
            $whereDate = [];

            $dj_data = DJModel::WHERE('id', $contestant_id)->PLUCK('name');
            if ($contestant_id)
                array_push($where, ['votes.dj_id', '=', $contestant_id]);
            
            if ($date)
                $query = Votesmodel::orderBy('created_at')
                        ->WHERE($where)
                        ->whereDate('votes.created_at', '=', Carbon::parse($date)->format('Y-m-d'))
                        ->get()
                        ->groupBy(function($data) {
                            return \Carbon\Carbon::parse($data->created_at)->format('Y-m-d');
                        })
                        ->map(function($entries) {
                            return $entries->count();
                        })
                        ->toArray();
            else             
                $query = Votesmodel::orderBy('created_at')->WHERE($where)->get()->groupBy(function($data) {
                    return \Carbon\Carbon::parse($data->created_at)->format('Y-m-d');
                })
                ->map(function($entries) {
                    return $entries->count();
                })
                ->toArray();

            return $this->buildJson(['votes'=>$query, 'contestant' => $dj_data]);
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getAllContestantVotes(Request $request){
        $queryRequest   = array_slice($request->all(), 3);
        $field          = ($queryRequest) ? explode('|', $request->sort)[0] : 'created_at';
        $direction      = ($queryRequest) ? explode('|', $request->sort)[1] : 'desc';
        $where = [];        
        array_push($where, ['dj_id', $request->id]);

        if ($request->search)
            array_push($where, ['ip_address', 'like', '%'.$request->search.'%']);

        return VotesModel::WHERE($where)        
                ->orderBy($field, $direction)
                ->PAGINATE($request->per_page);
    }
}
