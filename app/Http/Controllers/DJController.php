<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Models;
use App\Models\DJModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DJController extends Controller
{
    public function removeVideo(Request $request){
        $dj_data = DJModel::WHERE('id', $request->dj_id)->FIRST();
        $vid_string = "";

        $videos = explode("|",$dj_data->videos);
        unset($videos[$request->video]);
        
        foreach ($videos as $value) {
            $vid_string = ($vid_string == '') ? $value : $vid_string.'|'.$value;
        }
        $dj =DJModel::UPDATEORCREATE(
            [ 
                'id' => $request->dj_id
            ],
            [
                'videos' => $vid_string,
            ]
        );
    }
    public function getContestant(Request $request){
        return DJModel::WHERE('contest_id', '=', $request->contest_id)->GET();
    }
    public function getContestantInformation(Request $request){
        return DJModel::WHERE('id', '=', $request->contestant_id)->FIRST();
    }
    public function getAllContestant(Request $request){
        $where = [];
        array_push($where, ['dj.status','<', 2]);
        array_push($where, ['dj.contest_id', $request->id]);
        $djmodel = DJModel::SELECT(DB::RAW('CONCAT("https://contest.admin.ventures/uploads/",dj.profile_photo) as profile_photo_link, dj.*'))
                    ->WHERE($where)
                    ->orderByDesc('id')
                    ->GET();
        $votemodel = DB::table('dj')
                    ->select(DB::raw('COUNT(votes.id) as vote_count, dj.id as vote_id'))
                    ->where('dj.contest_id', '=', $request->id)
                    ->leftJoin('votes', 'dj.id', '=', 'votes.dj_id')
                    ->groupBy('dj.id')
                    ->orderBy('vote_count', 'desc')
                    ->get();
        
        return $this->buildJson(['contestant'=>$djmodel, 'votes' => $votemodel]);

    }
    public function getAllDJ(Request $request){
        $queryRequest   = array_slice($request->all(), 3);
        $where = [];

        array_push($where, ['dj.status','<', 3]);
        if ($request->search)
            array_push($where, ['dj.name', 'like', '%'.$request->search.'%']);

        $field  = ($queryRequest) ? explode('|', $request->sort)[0] : 'created_at';
        $direction  = ($queryRequest) ? explode('|', $request->sort)[1] : 'desc';
        
        array_push($where, ['dj.contest_id', $request->id]);

        return DJModel::WHERE($where)
                ->ORDERBY($field,$direction)
                ->PAGINATE($request->per_page);
    }
    public function save(Request $request){
        $validator = Validator::make(
            $request->all(),
			[
                'name'          => 'required',
            ]
        );

        if($request->id){
            $path         = '';
            // Upload Profile Picture
            if(isset($request->new_profile_photo)){
                $file           = $request->new_profile_photo;
                $file_extension = $file->getClientOriginalExtension();
                $file_name      = time().'.'.$file_extension;
                $path           = 'uploads/'.$file_name;
                $save_photo     = Storage::disk('public')->put($path, file_get_contents($file));
            }
            $videos_array = ($request->videos == null) ? '' : $request->videos;
            if(isset($request->new_videos)){
                if (count($request->new_videos) > 0) {
                    foreach ($request->new_videos as $new_vid) {
                        $ext = $new_vid->getClientOriginalExtension();
                        $file_name      = explode('.',$new_vid->getClientOriginalName())[0].'-'.time().'.'.$ext;
                        $name = 'https://contest.admin.ventures/uploads/videos/'.$file_name;
                        $upload_path = 'uploads/videos/';
                        $upload_url = $upload_path.$name;
                        $new_vid->move(public_path($upload_path),$upload_url);
                        // array_push($videos_array, $name);
                        $videos_array = ($videos_array == '') ? $name : $videos_array.'|'.$name;
                    }
                }
            }
            
            $dj =DJModel::UPDATEORCREATE(
                [ 
                    'id'            => $request->id 
                ],
                [
                    'name'          => $request->name,
                    'profile_photo' => $path,
                    'career'        => $request->career,
                    'official_site' => $request->official_site,
                    'social_media'  => $request->social_media,
                    'contest_id'    => $request->contest_id,
                    'videos'        => $videos_array,
                    'video_link'    => $request->video_link,
                    'status'        => $request->status,
                ]
            );
        }
        else{
            $path         = '';
            $videos_array = '';

            if ($validator->fails())
                return response()->json($validator->errors(), 422);
            
            // Upload Profile Picture
            
            if(isset($request->profile_photo)){
                $file           = $request->profile_photo;                
                $file_extension = $file->getClientOriginalExtension();
                $file_name      = time().'.'.$file_extension;
                $path           = 'uploads/'.$file_name;
                $save_photo     = Storage::disk('public')->put($path, file_get_contents($file));
            }
    
            // Upload Videos            
            if(isset($request->videos)){
                $videos = $request->videos;
                if (count($videos) > 0) {
                    foreach ($videos as $vid) {
                        $ext         = $vid->getClientOriginalExtension();
                        $file_name   = explode('.',$vid->getClientOriginalName())[0].'-'.time().'.'.$ext;
                        $name        = 'https://contest.admin.ventures/uploads/videos/'.$file_name;
                        $upload_path = 'uploads/videos/';
                        $upload_url  = $upload_path.$name;
                        $vid->move(public_path($upload_path),$upload_url);
                        // array_push($videos_array, $name);
                        $videos_array = ($videos_array == '') ? $name : $videos_array.'|'.$name;
                    }
                }
                $save_videos    = true;
            }
            
            // Upload Information
            // if($save_photo && $save_videos){
                try {
                    $dj =DJModel::UPDATEORCREATE(
                        [ 
                            'id'            => $request->id 
                        ],
                        [
                            'name'          => $request->name,
                            'career'        => $request->career,
                            'profile_photo' => $path,
                            'official_site' => $request->official_site,
                            'social_media'  => $request->social_media,
                            'contest_id'    => $request->contest_id,
                            'videos'        => $videos_array,
                            'video_link'    => $request->video_link,
                        ]
                    );
                    if($dj)
                        return response()->json(['message' => 'DJ Uploaded'], 200);
                    else
                        return response()->json(['message' => 'There was a problem processing your request'], 500);
                }
                catch (\Exception $e) {
                    return response()->json(['message' => $e->getMessage()], 500);
                }
            // }
        }
    }
    public function delete(Request $request){
        $id = isset($request->id) ? $request->id : NULL;
        $dj = DJModel::WHERE('id', $id)->FIRST();        

        if(empty($dj))
            return response()->json(['message' => 'Invalid Reference Key'], 422);
        try{
            $dj_update = DJModel::UPDATEORCREATE(
                [ 'id' => $request->id ],
                [
                    'status'    => 3,                    
                ]
            );
            if($dj_update)
				return response()->json(['message' => 'DJ is removed'], 200);
			else
				return response()->json(['message' => 'There was a problem processing your request'], 500);
        }
        catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage()], 500);
		}
    }
}
