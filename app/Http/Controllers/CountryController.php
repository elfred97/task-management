<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\CountryModel;
use App\Models\ContestModel;

class CountryController extends Controller
{
    public function save(Request $request){
        $validator = Validator::make(
            $request->all(),
			[
                'name'         => 'required',
                'status'      => 'required',
            ]
        );
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        $social_media = '';
        foreach ($request->social_media as $soc){
            $social_media = ($social_media == '') ? $soc['name'] : $social_media.'|'.$soc['name'];
        }

        $where = [
            'name'         => filter_var($request->name,FILTER_SANITIZE_STRING),
            'social_media' => $social_media,
            'status'       => ($request->status == true) ? 1 : 0,
        ];

        try {
            $contest = CountryModel::UPDATEORCREATE(
                [ 'id' => $request->id ],
                $where
            );

            if($contest)
                return response()->json(['message' => 'Contest Informations is saved'], 200);
            else
                return response()->json(['message' => 'There was a problem processing your request'], 500);
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getAllCountries(){
        try {
            return $country = DB::table('country')->WHERE('status', 1)->GET();
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getCountry(Request $request){
        try {
            $contest = ContestModel::WHERE('id', $request->contest_id)->FIRST();
            return $country = DB::table('country')->WHERE('id', $contest->country_id)->GET();
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function getAllCountry(){
        try {
            return $country = DB::table('country')->WHERE('status','<', 3)->GET();
        }
        catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
    public function delete(Request $request){
        $id = isset($request->id) ? $request->id : NULL;
        $country = CountryModel::WHERE('id', $id)->FIRST(); 
        
        $contest = ContestModel::WHERE('country_id', '=', $id)->FIRST();

        if(empty($contest)){
            if(empty($country))
                return response()->json(['message' => 'Invalid Reference Key'], 422);
            try{
                $update_country = CountryModel::UPDATEORCREATE(
                    [ 'id' => $request->id ],
                    [
                        'status'    => 3,
                    ]
                );
                if($update_country)
                    return response()->json(['message' => 'Country is removed'], 200);
                else
                    return response()->json(['message' => 'There was a problem processing your request'], 500);
            }
            catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        else
            return response()->json(['message' => 'Country is used'], 500);
        
    }
}
