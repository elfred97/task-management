<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PayrollReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $payroll;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $payroll)
    {
        $this->user = $user;
        $this->payroll = $payroll;
        $this->subject = 'Payroll Reminder for ' . Carbon::now()->format('F Y');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.payroll_reminder');
    }
}
