<?php

namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContestModel extends Model
{
    public $table       = 'contest';
	public $timestamps  = TRUE;
	protected $fillable = [
		'name',
        'description',
		'date_from',
        'date_to',
        'status',
        'admin',
	];
}
