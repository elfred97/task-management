<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VotesModel extends Model
{
    public $table       = 'votes';
	public $timestamps  = TRUE;
	protected $fillable = [
		'dj_id',
		'contest_id',
        'ip_address'
	];
}
