# Task Management



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!
## Note:
Create a database 'task_manager';

execute following command:

```
npm install
composer update
php artisan migrate
php artisan db:seed --class=DatabaseSeeder

npm run watch
```
