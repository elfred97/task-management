<?php

// use App\Http\Controllers\InquiriesController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use \App\Http\Controllers\Scholars\HomeController;
use \App\Http\Controllers\ContestController;
use \App\Http\Controllers\DJController;
use \App\Http\Controllers\CountryController;
// use \App\Http\Controllers\CountryController;
// use \App\Http\Controllers\NotificationSettingControler;
// use \App\Http\Controllers\ReminderController;
// use \App\Http\Controllers\NotificationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/docs', function () {
    return view('documentation');
});

Route::get('/help', function () {
    return view('help');
});

Route::get('/voteDJ', 'VotesController@voteDJ');
Route::get('/getAllContestant', 'DJController@getAllContestant');

Route::middleware(['PreventBackHistory'])->group(function(){
    
    Route::middleware(['auth:admins'])->group(function(){
        
        Route::post('saveTask', [ContestController::class,'save'])->name('contest.save');
        Route::get('/getAllTask', 'ContestController@getAllContest');
        Route::get('/getContestInfo', 'ContestController@getContestInfo');
        Route::get('/getAllCountries', 'CountryController@getAllCountries');
        Route::post('saveDJ', [DJController::class,'save'])->name('dj.save');
        Route::post('/saveCountry', [CountryController::class,'save'])->name('dj.save');
        Route::get('/getCountry', 'CountryController@getCountry');
        Route::get('/getContestantInformation', 'DJController@getContestantInformation');        
        Route::get('/getTotalContestVotes', 'VotesController@getTotalContestVotes');
        Route::post('removeTask', [ContestController::class,'delete'])->name('dj.delete');
        Route::get('/getContestant', 'DJController@getContestant');
        Route::get('/getAllContestantVotes', 'VotesController@getAllContestantVotes');
        
        
        Route::post('/updateAccountInfo', 'GlobalController@updateAccountInfo');

        Route::get('/getStatuses', 'GlobalController@getStatuses');

        Route::get('/getType', 'GlobalController@getType');
        Route::post('/deleteType', 'GlobalController@deleteType');
        Route::post('/restoreType', 'GlobalController@restoreType');
        Route::post('/updateType', 'GlobalController@updateType');
        Route::post('/saveNewType', 'GlobalController@saveNewType');

        Route::get('/getUsers', 'GlobalController@getUsers');
        Route::post('/updateUser', 'GlobalController@updateUser');
        Route::post('/deleteUser', 'GlobalController@deleteUser');
        Route::post('/restoreUser', 'GlobalController@restoreUser');
        Route::post('/changePassword', 'GlobalController@changePassword');

        // Route::get('penalty-count/{type?}', 'PlayerController@getPenaltyCount');
        // Route::get('getLowestMMR', 'PlayerController@getLowestMMR');

        // Route::resource('reminders', 'ReminderController');
        // Route::post('newReminder',[ReminderController::class,'store']);
        // Route::post('updateReminder/{reminder}',[ReminderController::class,'update']);
        // Route::get('reminders/{reminder}/destroy',[ReminderController::class,'destroy']);
        // Route::get('getReminder',[ReminderController::class,'index']);

        Route::get('getPayrollHistory/{status}', 'GlobalController@getPayrollHistory');
        Route::post('updatePayrollHistory', 'GlobalController@updatePayrollHistory');
        Route::post('updatePendingPayrollHistory', 'GlobalController@updatePendingPayrollHistory');

        // Route::post('importPayroll', 'FileController@importPayroll');
        // Route::post('importZipQR', 'FileController@importZipQR');
    });


    //put here route that was accessed by both admin and scholars

    Route::middleware(['auth:admins,scholars'])->group(function(){        

        // Route::get('/notifications/{account_type}', [NotificationController::class,'index'])->name('index');

        Route::get('/getAccountInfo/{type}', 'GlobalController@getAccountInfo');

        Route::get('/','GlobalController@redirectMain');

        Route::get('/getScholarHistories','GlobalController@getScholarPlayingHistories');

        Route::get('/getListOfAccounts','GlobalController@getListAccounts');

        Route::get('/listOfCategory','GlobalController@getListofCategoryforPenalty');

        Route::match(['GET', 'POST'], '/logout', 'Auth\LoginController@logout');

        Route::middleware(['vue.components'])->group(function(){
            Route::get('/{route}', 'GlobalController@index'); //
            Route::get('/contestant/{id}', 'GlobalController@index'); //
            Route::get('/vote/{id}', 'GlobalController@index'); //
            Route::get('/vote/{id}/{contestant_id}', 'GlobalController@index'); //
        });
        Route::get('{any}', function () {
            return view('homepage');
        })->where('any','.*');
    });

});