Dear {{ $user->first_name ?? 'Scholar' }} <br><br>

I want to notify you about the payroll. <br>
<br>
<b>Total SLP</b>: {{ $payroll->total_slp }}
<br><br>

Note: this is a sample email for project development.
