Dear {{  $user->first_name ?? 'Scholar' }} <br><br>

{!! nl2br($reminder->description) !!}

Note: this is a sample email for project development.
