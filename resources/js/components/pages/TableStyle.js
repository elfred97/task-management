export const TableStyle = {
    tableClass: 'table table-striped table-bordered table-hover',
    loadingClass: 'loading',
    ascendingIcon: 'fa fa-caret-up',
    descendingIcon: 'fa fa-caret-down',
    ascendingClass: 'sorted-asc',
    descendingClass: 'sorted-desc',
    sortableIcon: 'grey sort icon',
    handleIcon: 'grey sidebar icon',

    // handleIcon: 'fa glyphicon-menu-hamburger',
    pagination: {
        infoClass: 'pull-left',
        wrapperClass: 'vuetable-pagination pull-right',
        activeClass: 'btn-primary',
        disabledClass: 'disabled',
        pageClass: 'btn btn-border',
        linkClass: 'btn btn-border',
        icons: {
        first: '',
        prev: '',
        next: '',
        last: '',
        },
    }
}