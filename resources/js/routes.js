export const routes = [
    { path: '*', redirect: '/home'},
    { 
        path: '/home', 
        component: require('./components/pages/HomeComponent.vue').default,
        meta : {
            admins : true,
        }
    },
    { 
        path: '/settings', 
        component: require('./components/pages/AccountComponent.vue').default,
        meta : {
            admins : true,
        }
    },
    
]